import psycopg2
import request

def main():
	#Remote config
	HOST="tuxa.sme.utc"
	USER="bdd0p109"
	PASSWORD="0MnvDFHr"
	DATABASE="dbbdd0p109"

	#Local Config
	# HOST="localhost"
	# USER="client"
	# PASSWORD="client"
	# DATABASE="na17p2"

	conn = psycopg2.connect("host='%s' dbname='%s' user='%s' password='%s'"%(HOST,DATABASE,USER,PASSWORD))
	try:
		cur = conn.cursor()

		idConnected=-1
		quit=False
		print("Bienvenue dans la vue client");
		while idConnected==-1:
			try:
				idClient = int(input("Veuillez entrer votre id client : "))
				sql = "SELECT COUNT(id),userInformation->>'Nom' AS nom,userInformation->>'Prenom' AS prenom FROM client WHERE id=%i GROUP BY nom,prenom" % idClient
				cur.execute(sql)
				data = cur.fetchone()
				if data != None:
					idConnected=data[0]
					print("Bonjour "+str(data[1])+" " +str(data[2]))
				else:
					print("L'identifiant n'appartient à personne")
			except psycopg2.IntegrityError as e:
				print("Message système : ", e)
			except ValueError as e:
				print("Erreur : Veuillez entrer une valeur strictement numérique")

		while not quit:
			selected=0
			print("Pour connaître la liste des applications compatibles avec vos appareils : 1")
			print("Pour avoir un historique des achat : 2")
			print("Pour avoir un historique des installations : 3")
			print("Pour gérer vos terminaux : 4")
			print("Information utilisateur : 5")
			print("Pour quitter l'application : 6")
			try:
				selected=int(input("Action : "))
			except ValueError as e:
				print("Erreur : Veuillez entrer une valeur strictement numérique")
			if selected>=1 and selected<=6:
				if selected==1:
					request.requestCompatible(cur,idClient)
				if selected==2:
					request.requestAchat(cur,idClient)
				if selected==3:
					request.requestInstallation(cur,idClient)
				if selected==4:
					request.requestTerminal(conn,cur,idClient)
				if selected==5:
					request.showUtilisateurInfo(cur,idClient)
				if selected==6:
					quit=True
		conn.close()
	except psycopg2.errors.InFailedSqlTransaction as e:
		print("Problème SQL", e)