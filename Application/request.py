import psycopg2

def requestCompatible(cur,idClient):
	try:
		sql = "SELECT titreApplication,terminal FROM applicationCompatible WHERE idClient=%i ORDER BY terminal,titreApplication" % idClient
		cur.execute(sql)
		data=cur.fetchone()
		if data==None:
			print("Aucune application de trouvée")
			input()
		while data:
			print("Application : "+str(data[0])+", Terminal : " +str(data[1]))
			input()
			data=cur.fetchone()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)

def requestAchat(cur,idClient):
	try:
		sql = "SELECT titre,applicationLie,type,idReceveur FROM historiqueAchat WHERE idClient=%i ORDER BY type,titre" % idClient
		cur.execute(sql)
		data=cur.fetchone()
		if data==None:
			print("Aucun achat de trouvé")
			input()
		while data:
			line="Type : "+str(data[2]) + ", Titre : "+ str(data[0])
			if data[2]=="Ressource":
				line=line+", Application Lié : " + data[1]
			line=line+", idReceveur : " + str(data[3])
			print(line)
			input()
			data=cur.fetchone()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)

def requestInstallation(cur,idClient):
	try:
		sql="SELECT titre,applicationlie,type,idTerminal FROM historiqueInstallation WHERE idClient=%i ORDER BY idTerminal,type,titre" % idClient
		cur.execute(sql)
		data=cur.fetchone()
		if data==None:
			print("Aucune installation de trouvée")
			input()
		while data:
			line="Type : "+str(data[2]) + ", Titre : "+ str(data[0])
			if data[2]=="Ressource":
				line=line+", Application Lié : " + data[1]
			line=line+", Terminal : " + str(data[3])
			print(line)
			input()
			data=cur.fetchone()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)

def requestTerminal(conn,cur,idClient):
	quit=False
	while not quit:
		print("Pour afficher vos terminaux : 1")
		print("Pour renommer un terminal : 2")
		print("Pour ajouter un terminal : 3")
		print("Pour retourner au menu principal : 4")
		try:
			choice = int(input())
			if choice >=1 and choice<=4:
				if choice==1:
					showAllTerminaux(cur,idClient)
				if choice==2:
					updateTerminal(conn,cur,idClient)
				if choice==3:
					addTerminal(conn,cur,idClient)
				if choice==4:
					quit=True
		except ValueError as e:
			print("Erreur : Veuillez entrer une valeur strictement numérique")

def showAllTerminaux(cur,idClient):
	try:
		sql="SELECT nomTerminal,numeroSerie,nomModele FROM ClientTerminaux WHERE idClient=%i ORDER BY nomTerminal" % idClient
		cur.execute(sql)
		data=cur.fetchone()
		if data==None:
			print("Aucun terminal de trouvé")
			input()
		while data:
			print("Nom : " + str(data[0])+", NumeroSerie : "+str(data[1]) + ", Modèle : "+str(data[2]))
			input()
			data=cur.fetchone()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)

def updateTerminal(conn,cur,idClient):
	notValid=True
	while notValid==True:
		idTerminal=""
		nomTerminal=""
		try:
			idTerminal = int(input("Numéro de série du terminal : "))
			nomTerminal = input("Nouveau nom : ")
			if nomTerminal and idTerminal:
				notValid=False
			else:
				print("Des valeurs ne sont pas initialisées")
		except ValueError as e:
			print("Erreur : Veuillez entrer une valeur strictement numérique")

	try:
		sql="UPDATE ClientTerminaux SET nomTerminal='%s' WHERE idClient=%i AND numeroSerie='%i'" % (nomTerminal,idClient,idTerminal)
		cur.execute(sql)
		data = cur.rowcount
		if data==1:
			conn.commit()
			print("Terminal %s a bien été renommé %s" % (idTerminal,nomTerminal))
			input()
		else:
			print("Aucun terminal de modifier. Avez-vous fait une erreur dans le numero de série?")
			input()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)
	except psycopg2.DataError as e:
		print("Message personnalisé : Contrainte non respectée")
		print("Message système :", e)

def addTerminal(conn,cur,idClient):
	notValid=True
	while notValid==True:
		idTerminal=""
		nomTerminal=""
		idModele=""
		try:
			nomTerminal = input("Nom du terminal : ")
			idTerminal = int(input("Numero de serie : "))
			idModele = int(input("Identifiant du modèle : "))
			if nomTerminal and idTerminal and idModele:
				notValid=False
			else:
				print("Des valeurs ne sont pas initialisées")
		except ValueError as e:
			print("Erreur : Veuillez entrer une valeur strictement numérique")
	try:
		sql="INSERT INTO ClientTerminaux(nomTerminal,idModele,idClient,numeroSerie) VALUES('%s',%i,'%s','%i')" % (nomTerminal,idModele,idClient,idTerminal)
		cur.execute(sql)
		data = cur.rowcount
		if data==1:
			conn.commit()
			print("Le terminal a bien été ajouté")
			input()
		else:
			print("Aucun terminal d'ajouté")
			input()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)
	except psycopg2.DataError as e:
		print("Message personnalisé : Contrainte non respectée")
		print("Message système :", e)

def showUtilisateurInfo(cur,idClient):
	import json
	try:
		sql="SELECT id,username,userInformation FROM Client WHERE id=%i" % idClient
		cur.execute(sql)
		data=cur.fetchone()
		if data==None:
			print("Erreur : aucune information de trouvée")
			input()
		while data:
			dict_obj = json.loads(json.dumps(data[2]))
			print("Id : " + str(data[0])+", username : "+str(data[1])+", prénom : "+str(dict_obj.get('Prenom'))+", nom : "+str(dict_obj.get('Nom'))+", mail : "+str(dict_obj.get('Mail'))+", date inscription : "+str(dict_obj.get('DateInscription')))
			input()
			data=cur.fetchone()
	except psycopg2.IntegrityError as e:
			print("Message système : ", e)
