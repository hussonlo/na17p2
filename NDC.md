# NA17P2 : Sujet N°30 - Nimpstore

**HUSSON Loïc**

## Objets de la BDD et Propriétés
* Client
    * Nom : chaîne de caractères
    * Prenom : chaîne de caractères
    * Email : chaîne de caractères
    * DateInscription : Date
    * NumeroClient : entier
* Terminal
    * Numéro de série : chaîne de caractères
    * Nom de terminal : chaîne de caractères
    * Modèle : Modèle
* Modèle
    * Numéro de modèle : Entier
    * Constructeur : Constructeur
    * Désignation commerciale : chaîne de caractères
    * OS : OS
* OS
    * Nom : chaîne de caractères
    * Constructeur : Constructeur
    * Version : chaîne de caractères
* Constructeur
    * idConstructeur : Entier
    * Nom : Chaîne de caractères
* Logiciel
    * Titre : chaîne de caractères
    * Version : chaîne de caractères
    * Description : chaîne de caractères
    * PrixAchat : double
    * PrixAbonnement
    * frequence
    * Editeur : Editeur
* Application
* Ressource
    * Application lié : Application
* Editeur
    * Nom : chaîne de caractères
    * Contact : chaîne de caractères
    * URL : chaîne de caractères
* DisponibleLogicielOS
    * Logiciel : Logiciel
    * OS : OS
* Mode Paiement
    * idModePaiement : Entier
* Carte prépayée
    * MontantIni : double
    * MontantCrt : double
    * DataValide : Date
* Carte bancaire
    * NumeroCarte : chaîne de caractères (hachage)
    * Emetteur : chaîne de caractères
    * DateValidite : Date
* Acquisition
    * idAcquisition : Entier
    * DateAcquisition : Date
* AcquisitionRessource
* AcquisitionApplication
* Achat
    * Receveur : Client
* Souscription
    * DateFinSouscription : Date
    * Formule Souscription : Formule
* Formule
    * idFormule : Entier 
* FormuleAuto 
* FormuleNbMois
    * nbMois : Entier
* InstallationApplication
    * note : Entier
    * commentaire : varchar(500)
    * DateInstallation : Date
    * Achat d'application concerné : AcquisitionApplication
* InstallationRessource 
    * Achat de ressource concerné : AcquisitionRessource
    * DateInstallation : Date
    * Application installée concerné : InstallationApplication

## Contraintes
* Client
    * NumeroClient : PK
    * Nom : not null
    * Prénom : not null
* Terminal
    * Numéro de série : PK
    * Modèle : FK not null
* Modèle
    * Numéro de modèle : PK
    * Désignation commercial : not null
    * Constructeur : FK not null
    * OS : FK not null
* OS
    * Nom : PK
    * Version : PK
    * Constructeur : FK not null
* Constructeur
    * idConstructeur : PK
    * nom : not null
* Logiciel
    * Titre : PK
    * PrixAchat, PrixAbonnement, frequence : not null
    * Editeur : FK
* Application
    * Hérite de Logiciel (héritage exclusif)
* Ressource
    * Hérite de Logiciel (héritage exclusif)
    * Application lié : FK
* Editeur 
    * Nom : PK
    * Contact : aucune
    * URL : aucune
* DisponibleLogicielOS
    * Logiciel et OS : PK composée
* Mode Paiement
    * idModePaiement : PK
* Carte prépayée
    * Hérite de Mode Paiement (héritage exclusif)
    * MontantIni, MontantCrt, DateValide : not null
* Carte bancaire
    * Hérite de Mode Paiement (héritage exclusif)
    * NumeroCarte : aucune (On permet ainsi à l'utilisateur de supprimer ses coordonnées bancaires tout en gardant une trace de l'achat)
* Acquisition
    * idAcquisition : PK
* AcquisitionRessource et AcquisitionApplication
    * Hérite de Acquisition (héritage exclusif)
* Achat et Souscription 
    * Hérite de Acquisition (héritage exclusif) : Une acquisition peut être à la fois une acquisitionRessource et un Achat
* Achat
    * Receveur : FK
* Souscription 
    * DateDebut : not null
    * Formule souscription : FK
* Formule
    * idFormule : PK
* FormuleAuto
    * Hérite de Formule (héritage exclusif)
* FormuleNbMois
    * Hérite de Formule (héritage exclusif)
    * nbMois : not null >0
* InstallationApplication
    * note : Comprise entre 0 et 5 inclus
    * commentaire : aucune
    * Achat d'application concerné : FK
* InstallationRessource
    * Achat de ressource concerné : FK
    * Application installé concerné : FK

## Rôles
* Utilisateur
    * Peut ajouter, modifier, voir et supprimer
        * Ses terminaux
        * Ses InstallationRessource
        * Ses InstallationApplication
        * Ses souscription
        * Ses ModePaiement
    * Peut ajouter et voir
        * Ses acquisition
        * Ses achats
    * Peut voir 
        * Les applications
        * Les ressources
        * Les InstallationApplication
* Administrateur
    * A un accès complet sur la base de données
* Analyste
    * Hérite de utilisateurs
    * Peut voir l'ensemble des acquisitions
    * Peut voir l'ensemble des InstallationRessource

## Fonctions
* Client
    * réaliser une acquisition
    * ajouter/modifier/supprimer un terminal
    * installer une application sur un terminal
    * ajouter/supprimer un mode de paiement
    * définir une formule de souscription
* Carte prépayée
    * Vérifier la date de validité