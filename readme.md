# Project Title

The Nimpstore Projet NA17 n°30

## Authors

* **HUSSON Loïc**

## Contains
* **NDC** : note de clarification au format md
* **diagrammePNG** : diagramme UML au format PNG (en utilisant PUML)
* **diagrammePUML** : diagramme UML au format PUML
* **diagrammePUML/JSON** : diagramme UML au format PUML pour l'imbraction JSON
* **diagrammeMCDMerise** : diagramme MCD Merise au format .loo (nécessite Looping)
* **SQL** : script de création, insertion et création de vue
* **SQL/SQL JSON** : script de création, insertion et création de vue pour l'imbrication JSON
* **Application** : Contient l'application python pour intéragir avec la base de données
    *  **main.py** : Contient la fonction main() pour lancer le programme
    *  **request.py** : Contient les fonctions de requêtes