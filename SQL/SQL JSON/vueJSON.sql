--Contient les applications compatibles avec les terminaux du client
DROP VIEW IF EXISTS applicationCompatible;
CREATE VIEW applicationCompatible (titreApplication,idClient,nomClient,prenomClient,terminal) AS
SELECT a.titre, c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom', t.numeroSerie
FROM APPLICATION a, LOGICIEL l, COMPATIBLE co, OS o, MODELE m, TERMINAL t, CLIENT c
WHERE c.id = t.id_client AND t.id_modele = m.id AND m.nom_os = o.nom AND a.titre = l.titreApplication AND l.id = co.logiciel_id AND o.nom = co.os_nom
ORDER BY a.titre;

--Contient les applications acheté par un client et le client cible
DROP VIEW IF EXISTS historiqueAchatApplication;
CREATE VIEW historiqueAchatApplication (titreApplication,idClient,nomAcheteur,prenomAcheteur,idReceveur,nomReceveur,prenomReceveur) AS
SELECT a.titre,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM APPLICATION a, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONAPPLICATION aa, Cartebancaire cb
WHERE ac.id_carteBancaire = cb.id AND cb.id_client=c.id AND ac.id = achat.id AND c2.id = achat.id_client AND ac.id = aa.id AND aa.titre_application = a.titre AND ac.id_carteBancaire IS NOT NULL
UNION
SELECT a.titre,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM APPLICATION a, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONAPPLICATION aa, Carteprepayee cp
WHERE ac.id_carteprepayee = cp.id AND cp.id_client=c.id AND ac.id = achat.id AND c2.id = achat.id_client AND ac.id = aa.id AND aa.titre_application = a.titre AND ac.id_carteprepayee IS NOT NULL
ORDER BY titre;

--Contient les ressources acheté par un client et le client cible
DROP VIEW IF EXISTS historiqueAchatRessource;
CREATE VIEW historiqueAchatRessource (titreRessource,idClient,nomAcheteur,prenomAcheteur,idReceveur,nomReceveur,prenomReceveur) AS
SELECT r.titre,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM RESSOURCE r, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONRESSOURCE ar, Cartebancaire cb
WHERE ac.id_carteBancaire = cb.id AND cb.id_client=c.id AND ACHAT.id = ac.id AND c2.id = ACHAT.id_client AND ac.id = ar.id AND ar.titre_ressource = r.titre
UNION
SELECT r.titre,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM RESSOURCE r, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONRESSOURCE ar, Carteprepayee cp
WHERE ac.id_carteBancaire = cp.id AND cp.id_client=c.id AND ACHAT.id = ac.id AND c2.id = ACHAT.id_client AND ac.id = ar.id AND ar.titre_ressource = r.titre
ORDER BY titre;

--Contient tout les achat fait par les clients
DROP VIEW IF EXISTS historiqueAchat;
CREATE VIEW historiqueAchat (titre,applicationLie,type,idClient,nomAcheteur,prenomAcheteur,idReceveur,nomReceveur,prenomReceveur) AS
SELECT a.titre,NULL,'Application',c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM APPLICATION a, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONAPPLICATION aa, Cartebancaire cb
WHERE ac.id_carteBancaire = cb.id AND cb.id_client=c.id AND ac.id = achat.id AND c2.id = achat.id_client AND ac.id = aa.id AND aa.titre_application = a.titre AND ac.id_carteBancaire IS NOT NULL
UNION
SELECT a.titre,NULL,'Application',c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM APPLICATION a, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONAPPLICATION aa, Carteprepayee cp
WHERE ac.id_carteprepayee = cp.id AND cp.id_client=c.id AND ac.id = achat.id AND c2.id = achat.id_client AND ac.id = aa.id AND aa.titre_application = a.titre AND ac.id_carteprepayee IS NOT NULL
UNION
SELECT r.titre,r.applicationlie,'Ressource',c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM RESSOURCE r, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONRESSOURCE ar, Cartebancaire cb
WHERE ac.id_carteBancaire = cb.id AND cb.id_client=c.id AND ACHAT.id = ac.id AND c2.id = ACHAT.id_client AND ac.id = ar.id AND ar.titre_ressource = r.titre
UNION
SELECT r.titre,r.applicationlie,'Ressource',c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',c2.id,c2.userInformation->>'Nom',c2.userInformation->>'Prenom'
FROM RESSOURCE r, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONRESSOURCE ar, Carteprepayee cp
WHERE ac.id_carteBancaire = cp.id AND cp.id_client=c.id AND ACHAT.id = ac.id AND c2.id = ACHAT.id_client AND ac.id = ar.id AND ar.titre_ressource = r.titre
ORDER BY titre;

--Contient l'historique des installations application du client
DROP VIEW IF EXISTS historiqueInstallationApplication;
CREATE VIEW historiqueInstallationApplication (titre,idClient,nomClient,prenomClient,idTerminal) AS
SELECT a.titre,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',t.numeroSerie
FROM APPLICATION a, CLIENT c,ACQUISITION ac, ACQUISITIONAPPLICATION aa, INSTALLATIONAPPLICATION ia, TERMINAL t, estInstalleApplication e
WHERE c.id = t.id_client AND e.id_terminal = t.numeroSerie AND e.id_installationApplication = ia.id AND ia.id = ac.id AND ac.id = aa.id AND aa.titre_application = a.titre
ORDER BY titre;

--Contient l'historique des installations ressource du client
DROP VIEW IF EXISTS historiqueInstallationRessource;
CREATE VIEW historiqueInstallationRessource (titre,idClient,nomClient,prenomClient,idTerminal) AS
SELECT r.titre,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',t.numeroSerie
FROM RESSOURCe r, CLIENT c,ACQUISITION ac, ACQUISITIONRESSOURCE ar, INSTALLATIONRESSOURCE ir, TERMINAL t,  estInstalleRessource e
WHERE c.id = t.id_client AND e.id_terminal = t.numeroSerie AND e.id_installationRessource = ir.id AND ir.id = ac.id AND ac.id = ar.id AND ar.titre_ressource = r.titre
ORDER BY titre;

DROP VIEW IF EXISTS historiqueInstallation;
CREATE VIEW historiqueInstallation (titre,applicationlie,type,idClient,nomClient,prenomClient,idTerminal) AS
SELECT a.titre,NULL,'Application',c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',t.numeroSerie
FROM APPLICATION a, CLIENT c,ACQUISITION ac, ACQUISITIONAPPLICATION aa, INSTALLATIONAPPLICATION ia, TERMINAL t, estInstalleApplication e
WHERE c.id = t.id_client AND e.id_terminal = t.numeroSerie AND e.id_installationApplication = ia.id AND ia.id = ac.id AND ac.id = aa.id AND aa.titre_application = a.titre
UNION
SELECT r.titre,r.applicationLie,'Ressource',c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',t.numeroSerie
FROM RESSOURCE r, CLIENT c,ACQUISITION ac, ACQUISITIONRESSOURCE ar, INSTALLATIONRESSOURCE ir, TERMINAL t,  estInstalleRessource e
WHERE c.id = t.id_client AND e.id_terminal = t.numeroSerie AND e.id_installationRessource = ir.id AND ir.id = ac.id AND ac.id = ar.id AND ar.titre_ressource = r.titre
ORDER BY titre;

--Contient les terminaux associé à un client
DROP VIEW IF EXISTS ClientTerminaux;
CREATE VIEW ClientTerminaux(idClient,nomClient,prenomClient,nomTerminal,numeroSerie,idModele,nomModele) AS
SELECT c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom',t.nomTerminal,t.numeroSerie,m.id,m.designationCommerciale
FROM CLIENT c, TERMINAL t, MODELE m
WHERE c.id = t.id_client AND t.id_modele = m.id
ORDER BY t.nomTerminal;

CREATE RULE INSERTTerminal AS ON INSERT TO ClientTerminaux DO INSTEAD INSERT INTO TERMINAL VALUES(NEW.numeroSerie,NEW.nomTerminal,NEW.idClient,NEW.idModele);
CREATE RULE UPDATETerminal AS ON UPDATE TO ClientTerminaux DO INSTEAD UPDATE TERMINAL SET nomTerminal = NEW.nomTerminal WHERE numeroSerie=OLD.numeroSerie AND id_client=OLD.idClient;
CREATE RULE DELETETerminal AS ON DELETE TO ClientTerminaux DO INSTEAD DELETE FROM TERMINAL WHERE numeroSerie = OLD.numeroSerie AND nomTerminal = OLD.nomTerminal AND id_client = OLD.idClient;

-----------------------------------------------------------------


--Affiche les logiciels
DROP VIEW IF EXISTS showSoftware;
CREATE VIEW showSoftware(idLogiciel,type,titre,applicationlie,version,description,prixAchat,prixAbonnement,frequence,editeur) AS
SELECT id,'Application',titreApplication,NULL,version,description,prixAchat,prixAbonnement,frequence,editeur
FROM LOGICIEL
WHERE titreApplication IS NOT NULL
UNION
SELECT id,'Ressource',titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur
FROM LOGICIEL
WHERE titreRessource IS NOT NULL;

--Affiche les applications
DROP VIEW IF EXISTS manageApplication;
CREATE VIEW manageApplication(idLogiciel,titre,version,description,prixAchat,prixAbonnement,frequence,editeur) AS
SELECT id,titreApplication,version,description,prixAchat,prixAbonnement,frequence,editeur
FROM LOGICIEL
WHERE titreApplication IS NOT NULL;

--Affiche les ressources
DROP VIEW IF EXISTS manageRessource;
CREATE VIEW manageRessource(idLogiciel,titre,applicationlie,version,description,prixAchat,prixAbonnement,frequence,editeur) AS
SELECT id,titreApplication,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur
FROM LOGICIEL
WHERE titreRessource IS NOT NULL;

CREATE RULE INSERTApplication AS ON INSERT TO manageApplication DO INSTEAD(INSERT INTO APPLICATION VALUES(NEW.titre); INSERT INTO LOGICIEL(titreApplication,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES(NEW.titre,NEW.version,NEW.description,NEW.prixAchat,NEW.prixAbonnement,NEW.frequence,NEW.editeur));
CREATE RULE INSERTRessource AS ON INSERT TO manageRessource DO INSTEAD(INSERT INTO RESSOURCE VALUES(NEW.titre,NEW.applicationlie); INSERT INTO LOGICIEL(titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES(NEW.titre,NEW.applicationlie,NEW.version,NEW.description,NEW.prixAchat,NEW.prixAbonnement,NEW.frequence,NEW.editeur));

--Affiche les éditeurs
DROP VIEW IF EXISTS manageEditeur;
CREATE VIEW manageEditeur(nom,contact,url) AS
SELECT nom, editeurInfomation->>'Contact', editeurInfomation->>'URL'
FROM EDITEUR;

CREATE RULE INSERTEditeur AS ON INSERT TO manageEditeur DO INSTEAD INSERT INTO EDITEUR VALUES(NEW.nom,CAST('{"Contact":"'||NEW.contact||'","URL":"'||NEW.URL||'"}' AS JSON));

--Affiche les cartes prepayee et leurs informations
DROP VIEW IF EXISTS manageCartePrepayee;
CREATE VIEW manageCartePrepayee(id,montantInitial,montantActuel,dateValidite,idClient,nomClient,prenomClient) AS
SELECT cp.id,cp.montantIni,cp.montantAct,cp.dateValide,c.id,c.userInformation->>'Nom',c.userInformation->>'Prenom'
FROM Carteprepayee cp, Client c
WHERE cp.id_client = c.id;

CREATE RULE INSERTcarteprepayee AS ON INSERT TO manageCartePrepayee DO INSTEAD(INSERT INTO Carteprepayee(id_client,montantIni,montantAct,dateValide) VALUES(NEW.idClient,NEW.montantInitial,NEW.montantActuel,NEW.dateValidite));

-------------------------------------------------------------


CREATE OR REPLACE FUNCTION testDate(dateDeb DATE)
RETURNS DATE AS $dateFin$
	DECLARE 
		dateFin DATE;
		intervalle interval;
	BEGIN
		SELECT f.nbmois INTO intervalle FROM FORMULENBMOIS f;
		dateFin = dateDeb+ intervalle;
		RETURN dateFin;
	END;
$dateFin$ LANGUAGE plpgsql;


--calcul l'argent rapporté par l'application
CREATE OR REPLACE FUNCTION calculSommeApplication (id_application TEXT)
RETURNS MONEY AS $sommeArgentApplication$
    DECLARE
    	--Relative a l'argent
		argent MONEY=0;
		argent_one_step MONEY;
		nbMois INT;

		id_acquisition INT;
		idAchat INT;
		idSouscription INT;

		dateDepart DATE;
		dateFin DATE;
		dureeNonauto INTERVAL;
		dateDiff INT;
		idAuto INT;
		idnbmois INT;

		cursApplication CURSOR FOR SELECT ap.id FROM ACQUISITIONAPPLICATION ap WHERE titre_application = id_application;
	BEGIN
		OPEN cursApplication;
		FETCH NEXT FROM cursApplication INTO id_acquisition;
		WHILE id_acquisition IS NOT NULL
		LOOP
			idAchat=NULL;
			idSouscription=NULL;
			SELECT ac.id,s.id,a.acquisitionDate INTO idAchat,idSouscription,dateDepart FROM ACQUISITION a LEFT JOIN ACHAT ac ON ac.id=a.id LEFT JOIN SOUSCRIPTION s ON s.id=a.id WHERE a.id=id_acquisition;
			IF idAchat IS NOT NULL
				THEN
					SELECT l.prixAchat INTO argent_one_step FROM LOGICIEL l, APPLICATION a WHERE l.titreApplication = id_application;
					argent = argent+argent_one_step;
				ELSE
					--Cas souscription
					SELECT l.prixAbonnement, l.frequence INTO argent_one_step,nbMois FROM LOGICIEL l, APPLICATION a WHERE l.titreApplication = id_application;
					SELECT s.id_formuleauto,s.id_formulenbmois INTO idAuto,idnbmois FROM SOUSCRIPTION s WHERE s.id=id_acquisition;
					--Cas formule auto
					IF idAuto IS NOT NULL
						THEN
							SELECT f.dateFin INTO dateFin FROM FormuleAuto f WHERE f.id = idAuto;
							--Cas formule auto termine
							IF dateFin IS NOT NULL
								THEN
									dateFin = dateDepart+(dateFin-dateDepart);
								--Cas formule auto non termine
								ELSE
									dateFin = DATE(NOW());
									raise notice 'Value: %', dateFin;

							END IF;
						--Cas formule nb mois
						ELSE
							SELECT f.nbMois INTO dureeNonauto FROM FORMULENBMOIS f WHERE f.id = idnbmois;
								dateFin = dateDepart + dureeNonauto;
					END IF;
				WHILE TO_CHAR(AGE(dateFin,dateDepart),'DDD')::INTEGER > 0
				LOOP
					argent=argent+argent_one_step;
					dateDepart = dateDepart+make_interval(months := nbMois);
				END LOOP;
			END IF;
			FETCH NEXT FROM cursApplication INTO id_acquisition;
		END LOOP;
		CLOSE cursApplication;
		RETURN argent;
	END;
$sommeArgentApplication$ LANGUAGE plpgsql;

--calcul l'argent rapporté par la ressource
CREATE OR REPLACE FUNCTION calculSommeRessource(id_ressource TEXT)
RETURNS MONEY AS $sommeArgentRessource$
    DECLARE
    	--Relative a l'argent
		argent MONEY=0;
		argent_one_step MONEY;
		nbMois INT;

		id_acquisition INT;
		idAchat INT;
		idSouscription INT;

		dateDepart DATE;
		dateFin DATE;
		dureeNonauto INTERVAL;
		dateDiff INT;
		idAuto INT;
		idnbmois INT;

		cursRessource CURSOR FOR SELECT ar.id FROM ACQUISITIONRESSOURCE ar WHERE titre_ressource = id_ressource;
	BEGIN
		OPEN cursRessource;
		FETCH NEXT FROM cursRessource INTO id_acquisition;
		WHILE id_acquisition IS NOT NULL
		LOOP
			idAchat=NULL;
			idSouscription=NULL;
			SELECT ac.id,s.id,a.acquisitionDate INTO idAchat,idSouscription,dateDepart FROM ACQUISITION a LEFT JOIN ACHAT ac ON ac.id=a.id LEFT JOIN SOUSCRIPTION s ON s.id=a.id WHERE a.id=id_acquisition;
			IF idAchat IS NOT NULL
				THEN
					SELECT l.prixAchat INTO argent_one_step FROM LOGICIEL l WHERE l.titreRessource = id_ressource;
					argent = argent+argent_one_step;
				ELSE
					--Cas souscription
					SELECT l.prixAbonnement, l.frequence INTO argent_one_step,nbMois FROM LOGICIEL l WHERE l.titreRessource = id_ressource;
					SELECT s.id_formuleauto,s.id_formulenbmois INTO idAuto,idnbmois FROM SOUSCRIPTION s WHERE s.id=id_acquisition;
					--Cas formule auto
					IF idAuto IS NOT NULL
						THEN
							SELECT f.dateFin INTO dateFin FROM FormuleAuto f WHERE f.id = idAuto;
							--Cas formule auto termine
							IF dateFin IS NOT NULL
								THEN
									dateFin = dateDepart+(dateFin-dateDepart);
								--Cas formule auto non termine
								ELSE
									dateFin = DATE(NOW());
									raise notice 'Value: %', dateFin;

							END IF;
						--Cas formule nb mois
						ELSE
							SELECT f.nbMois INTO dureeNonauto FROM FORMULENBMOIS f WHERE f.id = idnbmois;
								dateFin = dateDepart + dureeNonauto;
					END IF;
				WHILE TO_CHAR(AGE(dateFin,dateDepart),'DDD')::INTEGER > 0
				LOOP
					argent=argent+argent_one_step;
					dateDepart = dateDepart+make_interval(months := nbMois);
				END LOOP;
			END IF;
			FETCH NEXT FROM cursRessource INTO id_acquisition;
		END LOOP;
		CLOSE cursRessource;
		RETURN argent;
	END;
$sommeArgentRessource$ LANGUAGE plpgsql;

--Affiche les applications par ordre de rentabilité
DROP VIEW IF EXISTS showApplicationRentable;
CREATE VIEW showApplicationRentable(id,titre,rentabilitéMensuel) AS
SELECT l.id,l.titreApplication,calculSommeApplication(l.titreApplication) AS rentabilite FROM LOGICIEL l WHERE l.titreApplication IS NOT NULL
ORDER BY rentabilite DESC,l.titreApplication;

--Calcul les profit fait par l'éditeur (Somme des revenus des ressources et applications de l'éditeur)
CREATE OR REPLACE FUNCTION calculProfitEditeur(nomEditeur TEXT)
RETURNS MONEY AS $profitEditeur$
	DECLARE
		argent MONEY=0;
		argent_one_step MONEY;
		id_logiciel INT;
		id_application TEXT;
		id_ressource TEXT;
		cursEditeur CURSOR FOR SELECT l.id FROM LOGICIEL l WHERE l.editeur=nomEditeur;
	BEGIN
		OPEN cursEditeur;
		FETCH NEXT FROM cursEditeur INTO id_logiciel;
		WHILE id_logiciel IS NOT NULL
		LOOP
			SELECT l.titreApplication,l.titreRessource INTO id_application,id_ressource FROM LOGICIEL l WHERE l.id = id_logiciel;
			IF id_application IS NOT NULL
				THEN
					SELECT calculSommeApplication(id_application) INTO argent_one_step;
					argent = argent+argent_one_step;
				ELSE
					SELECT calculSommeRessource(id_ressource) INTO argent_one_step;
					argent = argent+argent_one_step;
			END IF;
			FETCH NEXT FROM cursEditeur INTO id_logiciel;
		END LOOP;
		CLOSE cursEditeur;
		RETURN argent;
	END;
$profitEditeur$ LANGUAGE plpgsql;

--Affiche l'argent gagné par l'éditeur
DROP VIEW IF EXISTS showProfitEditeur;
CREATE VIEW showProfitEditeur(nom,profit) AS
SELECT e.nom,calculProfitEditeur(e.nom) AS profit FROM EDITEUR e
ORDER BY profit DESC;

--Affiche le nombre de VENTE fait par l'éditeur
DROP VIEW IF EXISTS showNombreVenteEditeur;
CREATE VIEW showNombreVenteEditeur(nom,nbVente) AS
SELECT e.nom,COUNT(ac.id) AS nb
FROM LOGICIEL l LEFT JOIN APPLICATION a ON l.titreApplication=a.titre LEFT JOIN RESSOURCE r ON l.titreRessource = r.titre,ACQUISITION ac LEFT JOIN ACQUISITIONRESSOURCE ar ON ac.id=ar.id LEFT JOIN ACQUISITIONAPPLICATION ap ON ac.id=ap.id,EDITEUR e 
WHERE l.editeur=e.nom AND (r.titre=ar.titre_ressource OR a.titre=ap.titre_application)
GROUP BY e.nom
ORDER BY nb DESC;

--Affiche le nombre d'installation de l'application
DROP VIEW IF EXISTS applicationInstallationView;
CREATE VIEW applicationInstallationView(nom,nombre) AS
SELECT a.titre,COUNT(eia.id_installationApplication) AS nombre
FROM Application a,ACQUISITIONAPPLICATION ap, ACQUISITION ac, INSTALLATIONAPPLICATION ia, estInstalleApplication eia
WHERE a.titre=ap.titre_application AND ap.id=ac.id AND ac.id=ia.id AND ia.id = eia.id_installationApplication
GROUP BY a.titre
ORDER BY nombre DESC,a.titre;

--Affiche le nombre d'installation de la ressource
DROP VIEW IF EXISTS ressourceInstallationView;
CREATE VIEW ressourceInstallationView(nom,nombre) AS
SELECT r.titre,COUNT(eir.id_installationRessource) AS nombre
FROM Ressource r,ACQUISITIONRESSOURCE ar, ACQUISITION ac, INSTALLATIONRESSOURCE ir, estInstalleRessource eir
WHERE r.titre=ar.titre_ressource AND ar.id=ac.id AND ac.id=ir.id AND ir.id = eir.id_installationRessource
GROUP BY r.titre
ORDER BY nombre DESC,r.titre;

--Affiche les profits réalisés par le distributeur
DROP VIEW IF EXISTS profitDistributeur;
CREATE VIEW profitDistributeur(profit) AS
SELECT SUM(calculProfitEditeur(e.nom))*0.3 AS profit
FROM EDITEUR e;

--Affiche les profits réalisés par le service editeur
DROP VIEW IF EXISTS profitEditeur;
CREATE VIEW profitEditeur(profit) AS
SELECT SUM(calculProfitEditeur(e.nom))*0.7 AS profit
FROM EDITEUR e;

--Affiche les utilisateurs par ordre d'activité. Ignore les utilisateurs inactifs
DROP VIEW IF EXISTS actifUser;
CREATE VIEW actifUser(id,nom,prenom,nbCommentaire) AS
SELECT c.id,c.userInformation->>'Nom' AS nom,c.userInformation->>'Prenom' AS prenom, COUNT(ia.commentaire) AS nombre
FROM CLIENT c, TERMINAL t, estInstalleApplication eia, INSTALLATIONAPPLICATION ia
WHERE c.id=t.id_client AND t.numeroSerie = eia.id_terminal AND eia.id_installationApplication = ia.id AND ia.commentaire IS NOT NULL
GROUP BY c.id,nom,prenom
ORDER BY nombre DESC, c.id;


--Creation User Client et ajout des droits pour les vues correspondantes
CREATE USER client WITH PASSWORD 'client';
GRANT SELECT ON client,applicationCompatible,historiqueInstallationApplication,historiqueInstallationRessource,historiqueAchatRessource,historiqueInstallation,historiqueAchatApplication,historiqueAchat TO client;
GRANT SELECT,DELETE,INSERT,UPDATE ON ClientTerminaux TO client;

--Creation User Administrateur et ajout des droits pour les vues correspondantes
CREATE USER administrateur;
GRANT SELECT ON showSoftware,manageApplication,manageRessource,manageEditeur,manageCartePrepayee TO administrateur;
GRANT INSERT ON manageApplication,manageRessource,manageEditeur,manageCartePrepayee TO administrateur;

--Creation User Analyste et ajout des droits pour les vues correspondantes
CREATE USER Analyste;
GRANT SELECT ON actifUser,showApplicationRentable,showProfitEditeur,showNombreVenteEditeur,applicationInstallationView,ressourceInstallationView,profitDistributeur,profitEditeur TO Analyste;