INSERT INTO CLIENT(username,userInformation) VALUES ('JediSlayer','{"Nom":"Vador","Prenom":"Darth","Mail":"darth-vador@darkstar.com","DateInscription":"1963-01-01"}');
INSERT INTO CLIENT(username,userInformation) VALUES ('Gandalf','{"Nom":"Le blanc","Prenom":"Gandalf","Mail":"gandalf@mordor.com","DateInscription":"1982-05-22"}');
INSERT INTO CLIENT(username,userInformation) VALUES ('Temperic','{"Nom":"HUSSON","Prenom":"Loic","Mail":"loic.husson@etu.utc.fr","DateInscription":"1997-03-19"}');
INSERT INTO CLIENT(username,userInformation) VALUES ('NA17Teacher','{"Nom":"CROZAT","Prenom":"Stéphane","Mail":"stephane.crozat@utc.fr","DateInscription":"2020-06-03"}');

INSERT INTO CONSTRUCTEUR(nom) VALUES ('Microsoft');
INSERT INTO CONSTRUCTEUR(nom) VALUES ('Apple');
INSERT INTO CONSTRUCTEUR(nom) VALUES ('Samsung');
INSERT INTO CONSTRUCTEUR(nom) VALUES ('Asus');
INSERT INTO CONSTRUCTEUR(nom) VALUES ('Google');

INSERT INTO OS(nom,id_constructeur) VALUES ('Android lolipop 5.2','5');
INSERT INTO OS(nom,id_constructeur) VALUES ('Android lolipop 5.3','5');
INSERT INTO OS(nom,id_constructeur) VALUES ('IOS 8.3.5a','2');
INSERT INTO OS(nom,id_constructeur) VALUES ('IOS 1.0.1','2');

INSERT INTO MODELE(designationCommerciale,id_constructeur,nom_os) VALUES('Galaxy 5.3','3','Android lolipop 5.3');
INSERT INTO MODELE(designationCommerciale,id_constructeur,nom_os) VALUES('Iphone X','2','IOS 8.3.5a');
INSERT INTO MODELE(designationCommerciale,id_constructeur,nom_os) VALUES('ROG Strix','4','Android lolipop 5.2');
INSERT INTO MODELE(designationCommerciale,id_constructeur,nom_os) VALUES('Windows phone','1','IOS 1.0.1');
--
INSERT INTO TERMINAL(numeroSerie,nomTerminal,id_client,id_modele) VALUES('5864','myAndroid01','1','1');
INSERT INTO TERMINAL(numeroSerie,nomTerminal,id_client,id_modele) VALUES('4896','theRing','2','3');
INSERT INTO TERMINAL(numeroSerie,nomTerminal,id_client,id_modele) VALUES('9632','monTelephone','3','4');
INSERT INTO TERMINAL(numeroSerie,nomTerminal,id_client,id_modele) VALUES('4444','mon2emeTelephone','3','4');
INSERT INTO TERMINAL(numeroSerie,nomTerminal,id_client,id_modele) VALUES('6666','telPro','4','2');

INSERT INTO EDITEUR(nom,editeurInfomation) VALUES('smallCompany',NULL);
INSERT INTO EDITEUR(nom,editeurInfomation) VALUES('bethesda','{"Contact":"bethesda@contact.fr","URL":"bethesda.fr"}');
INSERT INTO EDITEUR(nom,editeurInfomation) VALUES('darkstargames','{"Contact":"contact@darkstargames.fr","URL":"darkstargames.fr"}');
INSERT INTO EDITEUR(nom,editeurInfomation) VALUES('nintendo','{"Contact":"nintendo@nintendo.fr","URL":"nintendo.com"}');

INSERT INTO APPLICATION(titre) VALUES('mario sunshine');
INSERT INTO APPLICATION(titre) VALUES('skyrim');
INSERT INTO APPLICATION(titre) VALUES('the force awakening');
INSERT INTO APPLICATION(titre) VALUES('indy game');

INSERT INTO RESSOURCE(titre,applicationlie) VALUES('dragon born','skyrim');
INSERT INTO RESSOURCE(titre,applicationlie) VALUES('dawnguard','skyrim');
INSERT INTO RESSOURCE(titre,applicationlie) VALUES('indy game ressource','indy game');
INSERT INTO RESSOURCE(titre,applicationlie) VALUES('the force rising','the force awakening');

INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES('mario sunshine',null,null,'1.0','mario',0,0,0,'nintendo');
INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES('skyrim',null,null,'8.2','the elders scrolls',50,5,1,'bethesda');
INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES('the force awakening',null,null,'5.0','star wars',10,2,5,'darkstargames');
INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES('indy game',null,null,'1.0','indy game',5,1,5,'smallCompany');

INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES(null,'dragon born','skyrim','8.2','the elders scrolls',30.5,5,1,'bethesda');
INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES(null,'dawnguard','skyrim','8.2','the elders scrolls',25,5,1,'bethesda');
INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES(null,'indy game ressource','indy game','1.0','indy game ressource',1,0.2,3,'smallCompany');
INSERT INTO LOGICIEL(titreApplication,titreRessource,ressourceApplicationLie,version,description,prixAchat,prixAbonnement,frequence,editeur) VALUES(null,'the force rising','the force awakening','5.0','star wars',5,2,2,'darkstargames');

INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (2,'Android lolipop 5.2');
INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (2,'Android lolipop 5.3');
INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (5,'Android lolipop 5.2');
INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (5,'Android lolipop 5.3');
INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (6,'Android lolipop 5.2');
INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (6,'Android lolipop 5.3');
INSERT INTO COMPATIBLE(logiciel_id,os_nom) VALUES (1,'IOS 8.3.5a');

INSERT INTO CARTEPREPAYEE(id_client,montantIni,montantAct,dateValide) VALUES (2,50,50,'2020-08-17');
INSERT INTO CARTEPREPAYEE(id_client,montantIni,montantAct,dateValide) VALUES (1,25,5,'2019-01-01');
INSERT INTO CARTEPREPAYEE(id_client,montantIni,montantAct,dateValide) VALUES (3,30,10,'2018-10-10');
INSERT INTO CARTEPREPAYEE(id_client,montantIni,montantAct,dateValide) VALUES (4,100,20,'2020-12-25');

INSERT INTO CARTEBANCAIRE(id_client,numeroCarte,emetteur,dateValide) VALUES (1,'hachage','VISA','2020-02-01');
INSERT INTO CARTEBANCAIRE(id_client,numeroCarte,emetteur,dateValide) VALUES (3,'sdfgg34867','MASTERCARD','2021-05-01');
INSERT INTO CARTEBANCAIRE(id_client,numeroCarte,emetteur,dateValide) VALUES (2,'ds4687gwsdv41','VISA','2019-01-01');
INSERT INTO CARTEBANCAIRE(id_client,numeroCarte,emetteur,dateValide) VALUES (4,'ycdftrcdjhbsdfs','MASTERCARD','2025-10-01');

INSERT INTO ACQUISITION(id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(null,1,'2020/01/01');
INSERT INTO ACQUISITION(id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(1,null,'2020/01/01');
INSERT INTO ACQUISITION(id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(null,2,'2020/01/01');
INSERT INTO ACQUISITION(id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(null,2,'2020/01/01');

INSERT INTO ACHAT(id,id_client) VALUES(1,1);
INSERT INTO ACHAT(id,id_client) VALUES(3,3);

INSERT INTO FORMULEAUTO(id) VALUES(1);
INSERT INTO FORMULENBMOIS(nbMois) VALUES('3 month');

INSERT INTO SOUSCRIPTION(id,id_formuleauto,id_formulenbmois) VALUES(2,1,null);
INSERT INTO SOUSCRIPTION(id,id_formuleauto,id_formulenbmois) VALUES(4,NULL,1);

INSERT INTO ACQUISITIONAPPLICATION(id,titre_application) VALUES(1,'the force awakening');
INSERT INTO ACQUISITIONAPPLICATION(id,titre_application) VALUES(3,'skyrim');

INSERT INTO ACQUISITIONRESSOURCE(id,titre_ressource,ressourceApplicationLie) VALUES(2,'indy game ressource','indy game');
INSERT INTO ACQUISITIONRESSOURCE(id,titre_ressource,ressourceApplicationLie) VALUES(4,'dragon born','skyrim');

INSERT INTO INSTALLATIONAPPLICATION(id,dateInstallation,note,commentaire) VALUES(1,'2020-05-05',NULL,NULL);
INSERT INTO INSTALLATIONAPPLICATION(id,dateInstallation,note,commentaire) VALUES(3,'2020-06-01',5,'je recommande');

INSERT INTO INSTALLATIONRESSOURCE(id,dateInstallation,id_installationApplication) VALUES(4,'2020-06-01',3);

INSERT INTO estInstalleApplication(id_installationApplication,id_terminal) VALUES(1,9632);
INSERT INTO estInstalleApplication(id_installationApplication,id_terminal) VALUES(3,4444);
INSERT INTO estInstalleApplication(id_installationApplication,id_terminal) VALUES(3,9632);
INSERT INTO estInstalleRessource(id_installationRessource,id_terminal) VALUES(4,9632);


--Achat d'une ressource
INSERT INTO ACQUISITION(id_carteBancaire,acquisitionDate) VALUES(1,'2020/01/01');
INSERT INTO ACHAT(id,id_client) VALUES(5,1);
INSERT INTO ACQUISITIONRESSOURCE(id,titre_ressource,ressourceApplicationLie) VALUES(5,'indy game ressource','indy game');

--Achat 2eme jeu
INSERT INTO ACQUISITION(id,id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(10,null,1,'2020/01/01');
INSERT INTO ACHAT(id,id_client) VALUES(10,1);
INSERT INTO ACQUISITIONAPPLICATION(id,titre_application) VALUES(10,'skyrim');

--Souscription skyrim
INSERT INTO ACQUISITION(id,id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(11,null,1,'2020/01/01');
INSERT INTO FORMULEAUTO(id) VALUES(2);
INSERT INTO SOUSCRIPTION(id,id_formuleauto) VALUES(11,2);
INSERT INTO ACQUISITIONAPPLICATION(id,titre_application) VALUES(11,'skyrim');

--Souscription auto avec fin
INSERT INTO ACQUISITION(id,id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(12,null,1,'2020/01/01');
INSERT INTO FORMULEAUTO(id,dateFin) VALUES(3,'2020/02/01');
INSERT INTO SOUSCRIPTION(id,id_formuleauto) VALUES(12,3);
INSERT INTO ACQUISITIONAPPLICATION(id,titre_application) VALUES(12,'skyrim');

--Souscription nbMois de 2 mois
INSERT INTO ACQUISITION(id,id_carteprepayee,id_carteBancaire,acquisitionDate) VALUES(13,null,1,'2020/01/01');
INSERT INTO FORMULENBMOIS(id,nbMois) VALUES(2,'2 month');
INSERT INTO SOUSCRIPTION(id,id_formulenbmois) VALUES(13,2);
INSERT INTO ACQUISITIONAPPLICATION(id,titre_application) VALUES(13,'skyrim');