--Contient les applications compatibles avec les terminaux du client
DROP VIEW IF EXISTS applicationCompatible;
CREATE VIEW applicationCompatible (titreApplication,idClient,terminal) AS
SELECT a.titre, c.id, t.numeroSerie
FROM APPLICATION a, LOGICIEL l, COMPATIBLE co, OS o, MODELE m, TERMINAL t, CLIENT c
WHERE c.id = t.id_client AND t.id_modele = m.id AND m.nom_os = o.nom AND a.titre = l.titreApplication AND l.id = co.logiciel_id AND o.nom = co.os_nom;

--Contient les applications acheté par un client et le client cible
DROP VIEW IF EXISTS historiqueAchatApplication;
CREATE VIEW historiqueAchatApplication (titreApplication,idClient,idReceveur) AS
SELECT a.titre,c.id,c2.id
FROM APPLICATION a, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONAPPLICATION aa
WHERE c.id = ac.id_client AND ACHAT.id = ac.id AND c2.id = ACHAT.id_client AND ac.id = aa.id AND aa.titre_application = a.titre

--Contient les ressources acheté par un client et le client cible
DROP VIEW IF EXISTS historiqueAchatRessource;
CREATE VIEW historiqueAchatRessource (titreRessource,idClient,idReceveur) AS
SELECT r.titre,c.id,c2.id
FROM RESSOURCE r, CLIENT c,CLIENT c2,ACQUISITION ac, ACHAT, ACQUISITIONRESSOURCE ar
WHERE c.id = ac.id_client AND ACHAT.id = ac.id AND c2.id = ACHAT.id_client AND ac.id = ar.id AND ar.titre_ressource = r.titre;

--Contient l'historique des installations application du client
DROP VIEW IF EXISTS historiqueInstallationApplication;
CREATE VIEW historiqueInstallationApplication (titreApplication,idClient,idTerminal) AS
SELECT a.titre,c.id,t.numeroSerie
FROM APPLICATION a, CLIENT c,ACQUISITION ac, ACQUISITIONAPPLICATION aa, INSTALLATIONAPPLICATIOn ia, TERMINAL t, estInstalleApplication e
WHERE c.id = ac.id_client AND ac.id = aa.id AND aa.titre_application = a.titre AND aa.id=ia.id AND ia.id = e.id_installationApplication AND e.id_terminal=t.numeroSerie AND t.id_client = c.id;

--Contient l'historique des installations ressource du client
DROP VIEW IF EXISTS historiqueInstallationRessource;
CREATE VIEW historiqueInstallationRessource (titreRessource,idClient,idTerminal) AS
SELECT r.titre,c.id,t.numeroSerie
FROM RESSOURCe r, CLIENT c,ACQUISITION ac, ACQUISITIONRESSOURCE ar, INSTALLATIONRESSOURCE ir, TERMINAL t,  estInstalleRessource e
WHERE c.id = ac.id_client AND ac.id = ar.id AND ar.titre_ressource = r.titre AND ar.id=ir.id AND ir.id = e.id_installationRessource AND e.id_terminal=t.numeroSerie AND t.id_client = c.id;

--Contient les terminaux associé à un client
DROP VIEW IF EXISTS ClientTerminaux;
CREATE VIEW ClientTerminaux(idClient,nomTerminal,numeroSerie,nomModele) AS
SELECT c.id,t.nomTerminal,t.numeroSerie,m.designationCommerciale
FROM CLIENT c, TERMINAL t, MODELE m
WHERE c.id = t.id_client AND t.id_modele = m.id;



